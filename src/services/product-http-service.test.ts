import Product from "@/models/Product";
import { assert } from "console";
import { isDeepStrictEqual } from "util";
import { beforeEach, describe, expect, it } from "vitest";
import ProductsHttpService from "./products-http-service";

describe("ProductsHttpService", () => {
    let productsHttpService: ProductsHttpService;
    let product: Product;

    beforeEach(() => {
        product = new Product({
            id: 5,
            name: "Test product",
            price: 6789,
            description: "Test description",      
        });
        productsHttpService = new ProductsHttpService();
    });

    it("should get product by id", () => {
        productsHttpService.getProductById(product.id).then((productResponse) => {
            expect(assert(isDeepStrictEqual(product, productResponse), "true"), "true");
        });
    })
})